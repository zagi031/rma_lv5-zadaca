package com.example.whereami

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.location.*
import android.media.AudioManager
import android.media.SoundPool
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import com.example.whereami.databinding.ActivityMapsBinding

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var binding: ActivityMapsBinding
    private lateinit var soundPool: SoundPool
    private var soundPoolIsLoaded = false
    private var soundID: Int = 0
    private lateinit var mMap: GoogleMap
    private lateinit var locationManager: LocationManager
    private var markerCurrentLocation: Marker? = null
    private lateinit var geoCoder: Geocoder
    private val locationPermission = Manifest.permission.ACCESS_FINE_LOCATION
    private val locationRequestCode = 10
    private val locationListener = object : LocationListener {
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
        override fun onLocationChanged(location: Location) {
            updateLocationDisplay(location)
        }
    }
    val REQUEST_IMAGE_CAPTURE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.progressCircular.visibility = View.VISIBLE
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        geoCoder = Geocoder(this)

        loadSound()

        binding.btnTakeAPicture.setOnClickListener {
            dispatchTakePictureIntent()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createNotificationChannels()
    }

    private fun loadSound() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.soundPool = SoundPool.Builder().setMaxStreams(1).build()
        } else {
            this.soundPool = SoundPool(1, AudioManager.STREAM_MUSIC, 0)
        }
        this.soundPool.setOnLoadCompleteListener { _, _, _ -> soundPoolIsLoaded = true }
        this.soundID = this.soundPool.load(this, R.raw.hit, 1)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapLongClickListener {
            if (soundPoolIsLoaded)
                this.soundPool.play(soundID, 1f, 1f, 1, 0, 1f)
            val bitmapDrawable = resources.getDrawable(R.drawable.marker) as BitmapDrawable
            val bitmap = bitmapDrawable.bitmap
            val smallIcon = Bitmap.createScaledBitmap(bitmap, 90, 90, false)
            val marker =
                MarkerOptions().position(it).icon(BitmapDescriptorFactory.fromBitmap(smallIcon))
            mMap.addMarker(marker)
        }
        trackLocation()
    }

    private fun trackLocation() {
        if (hasPermissionCompat(locationPermission)) {
            startTrackingLocation()
        } else {
            requestPermisionCompat(arrayOf(locationPermission), locationRequestCode)
        }
    }

    private fun startTrackingLocation() {
        Log.d("TAG", "Tracking location")
        val criteria: Criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE
        val provider = locationManager.getBestProvider(criteria, true)
        val minTime = 1000L
        val minDistance = 10.0F
        try {
            locationManager.requestLocationUpdates(
                provider!!,
                minTime,
                minDistance,
                locationListener
            )
        } catch (e: SecurityException) {
            Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            locationRequestCode -> {
                if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    trackLocation()
                else
                    Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateLocationDisplay(location: Location) {
        val position = LatLng(location.latitude, location.longitude)
        val zoomLevel = 15f
        markerCurrentLocation?.remove()
        markerCurrentLocation =
            mMap.addMarker(MarkerOptions().position(position).title("Here I Am"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, zoomLevel))
        val address = geoCoder.getFromLocation(location.latitude, location.longitude, 1)
        binding.tvLat.text = "Latitude: ${address[0].latitude}"
        binding.tvLon.text = "Longitude: ${address[0].longitude}"
        binding.tvCountry.text = "Country: ${address[0].countryName}"
        binding.tvPlace.text = "Place: ${address[0].locality}"
        binding.tvAddress.text = "Address: ${address[0].thoroughfare} ${address[0].subThoroughfare}"
        if (binding.progressCircular.isVisible)
            binding.progressCircular.visibility = View.GONE
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.example.android.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    lateinit var currentPhotoPath: String
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            displayNotification(currentPhotoPath)
        }
    }

    private fun displayNotification(photoPath: String) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.setDataAndType(Uri.parse(photoPath), "image/*")
        // We can put extra in the intent.
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val notification = NotificationCompat.Builder(this, getChannelId(CHANNEL_LIKES))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("New photo is saved!")
            .setContentText(photoPath)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .build()
        NotificationManagerCompat.from(this)
            .notify(1001, notification)
    }
}