package com.example.whereami

import android.app.Application
import android.content.Context

class WhereIAmApp: Application() {
    companion object {
        lateinit var context:Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}