package com.example.moviequotes.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.moviequotes.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(){

    private lateinit var binding: ActivityMainBinding
    private val viewModel by viewModel<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel.loadSounds()
        binding.ivSamuelLJackson.setOnClickListener {
            viewModel.playSound(it.id)
        }
        binding.ivAshtonKutcher.setOnClickListener {
            viewModel.playSound(it.id)
        }
        binding.ivGipsy.setOnClickListener {
            viewModel.playSound(it.id)
        }
        binding.ivBorat.setOnClickListener {
            viewModel.playSound(it.id)
        }
    }
}