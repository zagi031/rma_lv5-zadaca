package com.example.moviequotes

import android.app.Application
import android.content.Context
import com.example.moviequotes.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MovieQuotesApp: Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this

        startKoin {
            androidContext(this@MovieQuotesApp)
            modules(viewModelModule)
        }
    }
}