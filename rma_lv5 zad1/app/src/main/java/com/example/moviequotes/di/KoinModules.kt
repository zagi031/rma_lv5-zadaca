package com.example.moviequotes.di

import com.example.moviequotes.ui.MainActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel<MainActivityViewModel> { MainActivityViewModel() }
}