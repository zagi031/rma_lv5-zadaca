package com.example.moviequotes.utilities

import com.example.moviequotes.R

fun getSoundFromImageView(imageViewID: Int): Int {
    return when(imageViewID) {
        R.id.iv_ashtonKutcher -> R.raw.kevin
        R.id.iv_gipsy -> R.raw.biglebowski_gipsy
        R.id.iv_borat -> R.raw.borat
        R.id.iv_samuelLJackson -> R.raw.samuel_l_jackson
        else -> 0
    }
}