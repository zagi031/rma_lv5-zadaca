package com.example.moviequotes.ui

import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import androidx.lifecycle.ViewModel
import com.example.moviequotes.MovieQuotesApp
import com.example.moviequotes.R
import com.example.moviequotes.utilities.getSoundFromImageView

class MainActivityViewModel : ViewModel() {
    private val soundPool: SoundPool
    private var soundPoolIsLoaded = false
    private val soundMap: HashMap<Int, Int> = HashMap()

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.soundPool = SoundPool.Builder().setMaxStreams(1).build()
        } else {
            this.soundPool = SoundPool(1, AudioManager.STREAM_MUSIC, 0)
        }
        this.soundPool.setOnLoadCompleteListener { _, _, _ -> soundPoolIsLoaded = true }
    }

    fun loadSounds() {
        this.soundMap[R.raw.biglebowski_gipsy] =
            this.soundPool.load(MovieQuotesApp.context, R.raw.biglebowski_gipsy, 1)
        this.soundMap[R.raw.borat] =
            this.soundPool.load(MovieQuotesApp.context, R.raw.borat, 1)
        this.soundMap[R.raw.samuel_l_jackson] =
            this.soundPool.load(MovieQuotesApp.context, R.raw.samuel_l_jackson, 1)
        this.soundMap[R.raw.kevin] = this.soundPool.load(MovieQuotesApp.context, R.raw.kevin, 1)
    }

    fun playSound(imageViewID: Int) {
        if (soundPoolIsLoaded) {
            val soundID = this.soundMap[getSoundFromImageView(imageViewID)] ?: 0
            this.soundPool.play(soundID, 1f, 1f, 1, 0, 1f)
        }
    }
}